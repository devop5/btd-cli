#!/bin/bash

# Global config/vars
MASTERNAME="redismaster"
REDIS_1_PORT=16379
REDIS_2_PORT=16380
SENTINEL_1_PORT=26379 # not a configurable variable yet
SENTINEL_DOWNAFTER=300    # Timeout for an unreachable master before the sentinel fails over. Default: 30000 (30 sec)
SENTINEL_FAILOVERTIMEOUT=5000   # This is a timeout that the sentinel gives the new master to get into shape. Default: 180000 (3 min)


# Which OS? Using 'uname -s'...
# 'Linux' on linux, 'MINGW32_NT-6.2' on my windows gitbash, 'Darwin' for (unsupported) Mac
case $(uname -s) in
	"Linux")
		OS='Linux'
		;;
	"MING"*|"Cyg"*)
		OS='Windows'
		;;
	"Darwin")
		OS='MacOS'
		;;
	*)
		# Unknown...
		OS="$(uname -s)"
		;;
esac


# Methods of executing redis-cli (to differing levels of interactivity)
REDISCLI='redis-cli'
REDISCLI_INTERACTIVE='redis-cli'
REDISCLI_BASH='docker run --name redis-bash -t -i --rm=true redis bash'

# Set OS dependent vars
case $OS in
	"Linux")
		DOCKER_IP='127.0.0.1'
		# Assuming we have redis-cli on the linux machine, so don't need to use the docker proxy for the cli.
		;;
	"Windows")
		DOCKER_IP=$(boot2docker ip)
		# provides a short-lived version of another redis container for redis-cli operations
		REDISCLI='docker run --net=host --name redis_tmp -t --rm=true redis redis-cli'
		REDISCLI_INTERACTIVE='docker run --name redis_tmp -t -i --rm=true redis redis-cli'
		VBOXMANAGE="/c/Program Files/Oracle/VirtualBox/VBoxManage.exe"
		;;
	"MacOS")
		echo "Mac is untested, but should almost work as it has a boot2doocker available...."
		exit 1
		;;
	*)
		echo "$OS is unsupported."
		exit 1
		;;
esac


function show_usage {
	echo "Normal usage: "
	echo "  run          - starts new instances of redis, redis sentinel, rabbitmq (soon), mongodb (later)"
	echo "  clean        - removes all instances. Like, totally wiped."
	echo "  map_ports    - maps the boot2docker instance's ports to localhost. Only needs to be done once, and must briefly shutdown the VM first."
	echo
	echo "Redis debug: "
	echo "  stop_redis [#]  - stops the redis container(s). Defaults to all."
	echo "  start_redis [#] - starts the redis container(s). Defaults to all."
	echo "  sentinel        - initiates an interactive CLI session with the sentinel."
	echo "  sentinel (arg)  - queries sentinel with provided arg. eg. 'masters', 'master <mastername>', 'slaves'"
	echo "  querymaster     - queries sentinel on the current master via redis-cli."
	echo
	echo "Rabbit debug:"
	echo "  .."
	echo
	echo "Mongo debug:"
	echo "  .."
	echo
	echo "See the README.md for more usage notes."
}

function clean {
	# Removing existing instances
	#docker stop --time=0 redis_0 sentinel_0
	echo -n "Wiping: " && docker rm --force redis_1
	echo -n "Wiping: " && docker rm --force redis_2
	echo -n "Wiping: " && docker rm --force sentinel_1
}

# Creates (but doesn't start) the redis containers
function create_redis {
	echo -n "Creating redis_1: "
	docker create --net=host --name redis_1 -p $REDIS_1_PORT:$REDIS_1_PORT -t -i redis redis-server --port $REDIS_1_PORT
	echo -n "Creating redis_2: "
	docker create --net=host --name redis_2 -p $REDIS_2_PORT:$REDIS_2_PORT -t -i redis redis-server --port $REDIS_2_PORT

	echo -n "Creating sentinel_1: "
	# NOTE: The sentinel's base config is to look for a master on localhost:6379 by the name of 'mymaster'
	#       We will create another monitor for redis_1 and redis_2 with the name '$MASTERNAME' (in setup_redis())
	#       This COULD be overridden with another --sentinel arg, but currently separating out for clarity.
	docker create --net=host --name sentinel_1 -p $SENTINEL_1_PORT:$SENTINEL_1_PORT joshula/redis-sentinel --sentinel announce-ip $DOCKER_IP --sentinel announce-port $SENTINEL_1_PORT
}

# Starts redis server N (or all if not specified)
function start_redis {
	if [ -z $1 ] || [ $1 == '1' ]; then
		echo -n "Starting: " && docker start redis_1
		echo "   redis_1: $DOCKER_IP:$REDIS_1_PORT"
	fi

	if [ -z $1 ] || [ $1 == '2' ]; then
		echo -n "Starting: " && docker start redis_2
		echo "   redis_2: $DOCKER_IP:$REDIS_2_PORT"
	fi
}

function start_sentinel {
	# start sentinel instance
	echo -n "Starting: "
	docker start sentinel_1
	echo "   sentinel_1: $DOCKER_IP:$SENTINEL_1_PORT"
}

function setup_redis {
	echo -n "Setting slave as $DOCKER_IP:$REDIS_2_PORT to master $DOCKER_IP:$REDIS_1_PORT: "
	$REDISCLI -h $DOCKER_IP -p $REDIS_2_PORT slaveof $DOCKER_IP $REDIS_1_PORT

	echo -n "Setting sentinel_1 - monitor redis_1 (with the mastername of '$MASTERNAME') with sentinel quorum of 1: "
	$REDISCLI -h $DOCKER_IP -p $SENTINEL_1_PORT sentinel monitor $MASTERNAME $DOCKER_IP $REDIS_1_PORT 1

	echo -n "Setting sentinel_1 - down-after-milliseconds to $SENTINEL_DOWNAFTER ms: "
	$REDISCLI -h $DOCKER_IP -p $SENTINEL_1_PORT sentinel set $MASTERNAME down-after-milliseconds $SENTINEL_DOWNAFTER

	echo -n "Setting sentinel_1 - failover-timeout to $SENTINEL_FAILOVERTIMEOUT ms: "
	$REDISCLI -h $DOCKER_IP -p $SENTINEL_1_PORT sentinel set $MASTERNAME failover-timeout $SENTINEL_FAILOVERTIMEOUT
	#$RUNTHRUDOCKER redis-cli -h $DOCKER_IP -p $SENTINEL_1_PORT sentinel set  $MASTERNAME parallel-syncs 1
}



# Stops redis server N (or all if not specified)
function stop_redis {
	[ -z $1 -o $1 == '1' ] && echo -n "Stopping redis_1: " && docker stop redis_1
	[ -z $1 -o $1 == '2' ] && echo -n "Stopping redis_2: " && docker stop redis_2
}

function check_docker {
	# HACK: Check for SSH certificate error (only seems to affect boot2docker 1.7) and fix if encountered. Nige. 20150709
	# This will expose a buggy ssh cert.
	if [ $(docker run --rm=true hello-world 2>&1 | grep "certificate is valid for" --count) -eq 1 ]; then
	# This will fix it
		echo "** Detected ssh cert bug in boot2docker 1.7.   Restarting docker on VM to force new server cert..."
		boot2docker ssh "sudo /etc/init.d/docker restart"
		boot2docker shellinit
		echo "** Resolved..."
	fi
}


function talk_to_sentinel
{
	if [ -z $1 ]; then
		$REDISCLI_INTERACTIVE -h $DOCKER_IP -p 26379
	else
		$REDISCLI -h $DOCKER_IP -p 26379 SENTINEL $@
	fi
}

function redis_bash
{
	echo "Starting new redis container for /bin/bash..."
	$REDISCLI_BASH
}


function map_ports
{
	if [[ $OS == 'Linux' ]]; then
		echo 'Port mapping not required. Skipping...'
	else
		# Unable to perform port-mapping on live machine, even though it is possible through the UI....
		echo "** Shutting down boot2docker-vm for port mapping"
		boot2docker stop
		echo "** Performing port mapping for redis_1 16379:16379"
		"$VBOXMANAGE" modifyvm "boot2docker-vm" --natpf1 "redis1,tcp,,16379,,16379"
		echo "** Performing port mapping for redis_2 16380:16380"
		"$VBOXMANAGE" modifyvm "boot2docker-vm" --natpf1 "redis2,tcp,,16380,,16380"
		echo "** Performing port mapping for sentinel_1 26379:26379"
		"$VBOXMANAGE" modifyvm "boot2docker-vm" --natpf1 "sentinel1,tcp,,26379,,26379"
		echo "** Restarting boot2docker"
		boot2docker start
	fi
}


function run {
	# create redis instances
	create_redis

	# start all redis instances
	start_redis

	# start and setup the sentinel
	start_sentinel

	# Setup master/slave
	sleep 1
	setup_redis
}




echo "** btd-cli - The Boot2Docker CLI for dev environments - https://bitbucket.org/devop5/btd-cli - (DOCKER IP : $DOCKER_IP, OS=$OS)"

# Cleaning up leftover redis_tmp if existing
[ $(docker ps -a | grep --count redis_tmp) -gt 0 ] && echo -n "Wiping: " && docker rm --force redis_tmp


if [ -z $1 ]; then
	show_usage
elif [[ $1 == 'map_ports' || $1 == 'init' ]]; then
	map_ports
elif [ -z $DOCKER_IP ]; then
	echo "** Boot2Docker does not seem to be running. Please execute 'boot2docker.exe start'. Alternatively, 'boot2docker.exe shellinit' if you're certain it is working."
	exit 0
elif [ $1 == 'clean' ]; then
	clean
elif [ $1 == 'querymaster' ]; then
	talk_to_sentinel get-master-addr-by-name $MASTERNAME
elif [ $1 == 'sentinel' ]; then
	# create an interactive redis-cli to the sentinel by default
	if [ -z $2 ]; then
		talk_to_sentinel
	else
		shift
		# Useful commands: masters, master, slaves
		echo $@
		talk_to_sentinel $@
	fi

elif [ $1 == 'stop_redis' ]; then
	shift
	stop_redis $@
elif [ $1 == 'start_redis' ]; then
	shift
	start_redis $@
elif [ $1 == 'run' ]; then
	check_docker
	run
elif [ $1 == 'bash' ]; then
	redis_bash
else
	show_usage
fi

echo


# To monitor the redis and sentinel instances:
#   docker logs -f redis_0
#   docker logs -f sentinel
