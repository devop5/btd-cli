#!/bin/bash

WAITTIME=25

function setup {
    echo "***"
    echo "*** SETUP"
    echo "***"
    ./btd-cli.sh clean
    ./btd-cli.sh run
    sleep 15 # allow time to settle
}

function cycle_redis {
    echo "*** "
    echo "*** KILLING redis_$1"
    echo "*** "
    ./btd-cli.sh stop_redis $1
    sleep $WAITTIME

    echo "*** "
    echo "*** STARTING redis_$1"
    echo "*** "
    ./btd-cli.sh start_redis $1
    sleep $WAITTIME
}



setup
cycle_redis 1
cycle_redis 2
cycle_redis 1
cycle_redis 2
cycle_redis 1
cycle_redis 2
