# Boot2Docker CLI for Developer Environments

## Intro
This project provides a sandbox containerised environment for Windows developer machines. Specifically: redis (+sentinel), rabbitmq, and mongodb.

## Pre-requisites

* VirtualBox
* Boot2Docker
* [MSYS Git](https://msysgit.github.io/) (specifically for the git-bash)

All of the above can be installed in one step using [chocolatey](https://chocolatey.org/) via:

    choco install boot2docker git

## Usage

### First time only

1. Run `Start Boot2Docker` (from Windows start menu)
  * This will bring up the git bash shell to operate in.
2. Map the boot2docker vm's ports with `btd-cli.sh init`. This only needs to be done once, and will shutdown and restart the VM.

### Normal usage

1. Run `Start Boot2Docker` (from Windows start menu)
  * This will bring up the git bash shell to operate in.
2. Execute btd-cli.sh with the appropriate argument. eg. `btd-cli.sh run`

### Testing

There are two scripts to assist in testing.

1. `test-cycle.sh`
  - starts up a cluster, and alternates killing and bringing back up each of the masters.
2. `sentinel-test.py`
  - a python test script which infinitely attempts to hits the master (as defined by the sentinel), and each of the redis servers directly. 
  - desync's are also detected.

Running the `sentinel-test.py` script loop while using `test-cycle.sh` is the intended purpose.

## Notes
* The first execution will be quite slow, as docker images will be downloaded. This is a once-off (unless the boot2docker VM is totally removed).
* It is safe to execute multiple shells with Boot2Docker. It detects the instance, and sets the environment variables happily.
* Ports are mapped as followed:
  * `16379` - Redis Server 1
  * `16380` - Redis Server 2
  * `26379` - Redis Sentinel
  * `5672`  - RabbitMQ (*todo*)
  * `15672` - RabbitMQ admin console (*todo*)
  * `27017` - MongoDB (*todo*)


## Syntax

    btd-cli.sh <argument>


### Basic

* `run`   - starts new docker instances of *redis*, *redis sentinel*, *rabbitmq* (soon), *mongodb* (later) in separate containers.
* `clean` - removes all containers. Like, totally wiped.
* `init`  - performs once-off operations. eg. mapping the boot2docker instance's ports to the host.

### Redis debug
* `start_redis`   - starts all redis servers
* `start_redis 1` - starts the 1st redis server
* `start_redis 1` - starts the 2nd redis server
* `stop_redis`    - stops all redis servers
* `stop_redis 1`  - stops the 1st redis server
* `stop_redis 2`  - stops the 2nd redis server
* `querymaster`   - queries the sentinel for the current master
* `sentinel`      - starts an interactive session with the sentinel
  * `sentinel masters`   - queries the sentinel for the known masters
  * `sentinel master <MASTERNAME>`   - queries the sentinel for information on the master
  * `sentinel slaves <MASTERNAME>`   - queries the sentinel for information on the slaves

### RabbitMQ debug
*  ..

### Mongo debug
*  ..


## Advanced Usage

Extra insight on the instances can be obtained with the `docker logs` command.

Specifically:

    docker logs -f redis_1
    docker logs -f sentinel_1


The containers created by btd-cli are:
  - redis_1
  - redis_2
  - sentinel_1
  - ..
