from time import sleep
from redis.sentinel import Sentinel
from optparse import OptionParser
import redis


REDISCLUSTERNAME = 'redismaster'

test_value = 100
SLEEP_SUCCESS = 2.5
SLEEP_FAIL = 1.5


parser = OptionParser()
parser.add_option("-v", action="store_true", dest="verbose", default=False)
(options, args) = parser.parse_args()

print "Verbose: {}".format(options.verbose)

while True:
    sleep_time = SLEEP_SUCCESS   # reduces a lot when encountering errors

    results = {}
    # Dictionary to stores the results of the tests.
    # Key = 'servername-testname'. Value = 'True' or 'errorstring'.

    # Test connections
    test_value = str(int(test_value) + 1) # Storing as a str in Redis
    print "Testing sentinel/redis_1/redis_2 with {}".format(test_value)

    servers = [('sentinel', 26379), ('redis_1', 16379), ('redis_2', 16380)]
    current_master = ""
    for s in servers:
        server_label = ''
        try:
            # direct conn or thru sentinel?
            if s[0] == 'sentinel':
                # via redis
                server_label = 'SENTINEL/{}'.format('cluster')
                sentinel = Sentinel([('127.0.0.1', s[1])])
                r = sentinel.master_for(REDISCLUSTERNAME)
                result_master = sentinel.discover_master(REDISCLUSTERNAME)
                if result_master[1] == 16379:
                    current_master = 'redis_1'
                elif result_master[1] == 16380:
                    current_master = 'redis_2'
                else:
                    # ??
                    current_master = str(result_master)
            else:
                # direct connection
                server_label = '  DIRECT/{}'.format(s[0])
                r = redis.Redis(port=s[1])

            read_result = ""
            write_result = ""

            # If the sentinel, we'll try a write first.
            if s[0] == 'sentinel':
                try:
                    r.set('master_foo', test_value)
                    write_result = True
                except redis.RedisError as e:
                    write_result = "Could not write via sentinel: {}".format(e.message)
                results['sentinel-write'] = write_result

            # Only perform other tests if the sentinel-write was successful.
            if results['sentinel-write'] is True:
                # Try to read the sentinel's write
                try:
                    result = r.get('master_foo')
                    if result == test_value:
                        read_result = True
                    else:
                        read_result = "Read cache out-of-sync. {} != {}".format(result, test_value)
                except redis.RedisError as e:
                    read_result = "Read failed: {}".format(e.message)

                # If not the sentinel, try a write (slave-detection)
                if s[0] != 'sentinel':
                    try:
                        r.set('direct_foo', 'bar')
                        write_result = True
                    except redis.RedisError as e:
                        # Probably the slave if the connect worked
                        write_result = "Write failed: {}".format(e.message)
            else:
                read_result = 'Sentinel failed'
                if s[0] != 'sentinel':
                    write_result = 'Sentinel failed'

            # Output results of this test (performed each loop)
            if write_result is True and read_result is True:
                if s[0] == 'sentinel':
                    print "  {}: ok         (master is: {})".format(server_label, current_master)
                else:
                    print "  {}: ok".format(server_label)
            elif read_result is True:
                if options.verbose is True:
                    print "  {}: looks like slave (read success only) - '{}'".format(server_label, write_result)
                else:
                    print "  {}: slave".format(server_label)
            elif write_result is True:
                print "- {}: {}".format(server_label, read_result)
                sleep_time = SLEEP_FAIL
            else:
                if options.verbose is True:
                    print "- {}: read fail, write fail  - '{}' / '{}'".format(server_label, read_result, write_result)
                else:
                    print "  {}: down".format(server_label)
                sleep_time = SLEEP_FAIL

        except redis.RedisError as e:
            sleep_time = SLEEP_FAIL
            print "- {}: {}".format(server_label, e.message)
            results['sentinel-write'] = e.message

        if results['sentinel-write'] is not True and not options.verbose:
            # Skip the other tests if not verbose
            break

    print
    sleep(sleep_time)
